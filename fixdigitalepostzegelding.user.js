// ==UserScript==
// @name        FIX DIGITALE POSTZEGEL DING
// @namespace   nl.puckipedia.greasemonkey
// @description Omdat het nodig is!
// @include     https://digitalepostzegel.postnl.nl/*
// @version     1
// @grant       none
// ==/UserScript==
function main() {
  var text = document.querySelector('#headerbanner>div>div');
  text.innerHTML = text.innerHTML.replace(/<br ?\/?>/g, '');
  text.innerHTML += '<br><br>Multi-printer fix door <a href="http://puckipedia.nl/" target="_blank">:Puckipedia</a>.<br>Keuze: <b id="preference_printer">Geen!</b> <a href="javascript:resetPreference()">Kies andere printer</a><br><br>';
  var selectedPrinterName = '\n\n';
  var oldGetPrinters = dymo.label.framework.getLabelWriterPrinters;
  resetPreference = function () {
    setPreference();
  }
  function setPreference() {
    var printers = oldGetPrinters();
    var toShow = 'Selecteer een printer\n-------\n';
    for (var i = 0; i < printers.length; i++)
    {
      toShow += i + '. ' + printers[i].name + ' (' + printers[i].modelName + ', ' + (printers[i].isConnected ? 'verbonden)\n' : 'niet verbonden)\n');
    }
    selectedPrinterName = printers[parseInt(window.prompt(toShow, '0'), 10)].name;
    document.getElementById('preference_printer').innerHTML = selectedPrinterName;
  }
  dymo.label.framework.getLabelWriterPrinters = function ()
  {
    var printers = oldGetPrinters();
    if (selectedPrinterName == '\n\n') {
      setPreference();
    }
    for (var i = 0; i < printers.length; i++)
    {
      if (printers[i].name == selectedPrinterName)
      return [printers[i]];
    }
  }
}
var script = document.createElement('script');
script.appendChild(document.createTextNode('(' + main + ')();'));
(document.body || document.head || document.documentElement).appendChild(script);
